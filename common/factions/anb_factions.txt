########################################
# Adventurer Factions
########################################

#Fighting
adv_marchers =
{
	monarch_power = MIL
	always = yes

	modifier = 
	{
		diplomatic_upkeep = -1
		idea_cost = 0.1
		
		diplomatic_reputation = 1
		land_morale = 0.05
		prestige_from_land = 0.5
		land_forcelimit_modifier = 0.2
	}
}

#Colonising and developing provinces
adv_pioneers =
{
	monarch_power = DIP
	always = yes

	modifier = 
	{
		administrative_efficiency = -0.2
		global_manpower_modifier = -0.2
	
		development_cost = -0.1
		land_attrition = -0.1
		global_colonial_growth = 20
		colonist_placement_chance = 0.1
	}
}

#Trading
adv_fortune_seekers =
{
	monarch_power = ADM
	always = yes

	modifier = 
	{
		discipline = -0.05
		diplomatic_reputation = -1
		
		caravan_power = 0.2
		global_trade_goods_size_modifier = 0.10
		diplomatic_upkeep = 1
		build_cost = -0.10
	}
}

########################################
# Technocracy Factions
########################################

#Technology
tec_inventors =
{
	monarch_power = ADM
	always = yes

	modifier = 
	{
		all_power_cost = -0.05
		free_policy = 1
		global_institution_spread = 0.1
		
		global_tax_modifier = -0.2
	}
}

#Economy
tec_manufacturers =
{
	monarch_power = DIP
	always = yes

	modifier = 
	{
		global_trade_goods_size_modifier = 0.2
		build_time = -0.25
		global_ship_recruit_speed = -0.25
		
		land_maintenance_modifier = 0.1
		naval_maintenance_modifier = 0.1
	}
}

#Military
tec_military_engineers =
{
	monarch_power = MIL
	always = yes

	modifier = 
	{
		artillery_power = 0.2
		ship_durability = 0.1
		siege_ability = 0.1
		
		global_unrest = 2
	}
}

########################################
# Aelnar Factions
########################################

#Fighting
aelnar_blaiddtar =
{
	monarch_power = MIL
	trigger = {
		has_reform = enlightened_empire_reform
		tag = Z43
	}

	modifier = 
	{
		infantry_power = 0.1
		land_morale = 0.05
		free_leader_pool = 1
		land_maintenance_modifier = 0.15
		army_tradition = 0.5
	}
}

#Navy Supremacy
aelnar_cymscal =
{
	monarch_power = DIP
	trigger = {
		has_reform = enlightened_empire_reform
		tag = Z43
	}

	modifier = 
	{
		naval_morale = 0.05
		global_ship_trade_power = 0.25
		naval_forcelimit_modifier = 0.1
		navy_tradition = 0.5
		naval_maintenance_modifier = 0.15
	}
}

#Converting
aelnar_suir =
{
	monarch_power = ADM
	trigger = {
		has_reform = enlightened_empire_reform
		tag = Z43
	}

	modifier = 
	{
		diplomatic_reputation = -1
		
		culture_conversion_cost = -0.15
		global_missionary_strength = 0.025
		missionaries = 1
		tolerance_own = 1
		tolerance_heretic = -1
		tolerance_heathen = -1
	}
}