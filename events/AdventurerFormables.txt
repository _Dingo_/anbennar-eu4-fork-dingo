
namespace = marcher

#New Country Formed - default
country_event = {
	id = marcher.1
	title = marcher.1.t
	desc = marcher.1.d
	picture = STREET_SPEECH_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		#government_type = adventurer
		#government_rank = 2
	}
	
	option = {		# Despotic Monarchy
		name = "marcher.1.a"
		ai_chance = { 
			factor = 75
			modifier = {
				factor = 10
				tag = B07 #Sons of Dameria
				tag = B37 #Rogieria
			}
			modifier = {
				factor = 2
				OR = {
					tag = B05 #Pioneer's Guild
					tag = B53 #Nurcestir
					tag = B19 #Sword Covenant
					tag = Z37 #Covenblad
					tag = B04 #Order of the Ashen Rose
					tag = Z35 #Rosande
					tag = B11 #Company of the Thorn
					tag = B34 #Luciande
				}
			}
			modifier = {
				factor = 2
				faction_in_power = adv_marchers
			}
			modifier = {
				factor = 2
				num_of_cities = 5
			}
		}	
		if = {
			limit = { NOT = { government = monarchy } }
			change_government = monarchy
		}
		add_government_reform = adventurer_kingdom_reform
		
		#Dynasty name triggers for each country
		hidden_effect = {
			#Greentide Adventurers
			country_event = { id = flavor_luciande.1 days = 1 }
			country_event = { id = flavor_ancardia.1 days = 1 }
			country_event = { id = flavor_rogieria.1 days = 1 }
			country_event = { id = flavor_elikhand.1 days = 1 }
			country_event = { id = flavor_wyvernheart.1 days = 1 }
			country_event = { id = flavor_alenor.1 days = 1 }
			country_event = { id = flavor_stalbor.1 days = 1 }
			country_event = { id = flavor_ravenmarch.1 days = 1 }
			country_event = { id = flavor_araionn.1 days = 1 }
			country_event = { id = flavor_newshire.1 days = 1 }
			country_event = { id = flavor_estaire.1 days = 1 }
			country_event = { id = flavor_anbenland.1 days = 1 }
			country_event = { id = flavor_nurcestir.1 days = 1 }
			country_event = { id = flavor_esthil.1 days = 1 }
			country_event = { id = flavor_rosande.1 days = 1 }
			country_event = { id = flavor_hammerhome.1 days = 1 }
			country_event = { id = flavor_covenblad.1 days = 1 }
			country_event = { id = flavor_silvermere.1 days = 1 }
			
			#Aelantir Adventurers
			country_event = { id = flavor_sornicande.1 days = 1 }
			country_event = { id = flavor_neratica.1 days = 1 }
			country_event = { id = flavor_beggaston.1 days = 1 }
			country_event = { id = flavor_istranar.1 days = 1 }
			country_event = { id = flavor_nur_menibor.1 days = 1 }
			country_event = { id = flavor_freemarches.1 days = 1 }
			country_event = { id = flavor_pelodaire.1 days = 1 }
			country_event = { id = flavor_west_tipney.1 days = 1 }
			country_event = { id = flavor_new_havoral.1 days = 1 }
			country_event = { id = flavor_chippengard.1 days = 1 }
			country_event = { id = flavor_plumstead.1 days = 1 }
			country_event = { id = flavor_nur_dhanaenn.1 days = 1 }
			country_event = { id = flavor_tellumtir.1 days = 1 }
			country_event = { id = flavor_jaherkand.1 days = 1 }

		}
	}
	option = {		# Oligarchic Republic
		name = "marcher.1.b"
		ai_chance = { 
			factor = 30
			modifier = {
				factor = 1.5
				faction_in_power = adv_pioneers
			}
			modifier = {
				factor = 2	#Having a poor ruler makes it likely they want an elective country
				OR = {
					NOT = { adm = 3 }
					NOT = { dip = 3 }
					NOT = { mil = 3 }
				}
			}
		}
		if = {
			limit = { NOT = { government = republic } }
			change_government = republic
		}
		add_government_reform = adventurer_republic_reform
	}
	option = {		# Merchant Republic
		name = "marcher.1.c"
		ai_chance = { 
			factor = 10 
			modifier = {
				factor = 3
				faction_in_power = adv_fortune_seekers
			}
			modifier = {
				factor = 8
				OR = {
					tag = B09 #House of Riches
					tag = B49 #Araionn
				}
			}
		}
		if = {
			limit = { NOT = { government = republic } }
			change_government = republic
		}
		add_government_reform = merchants_reform
	}
	
	#Optional
	
	#Damerian Monarchy
	
	
	option = {		# Magocracy
		name = "marcher.1.dd"
		trigger = {
			OR = {
				tag = B20 #Iron Sceptre
				tag = B54 #Esthil
				ruler_has_personality = mage_personality
			}
		}
		ai_chance = { 
			factor = 10 
			modifier = {
				factor = 1000
				OR = {
					tag = B20 #Iron Sceptre
					tag = B54 #Esthil
				}
			}
			modifier = {
				factor = 3
				estate_influence = {
					estate = estate_mages
					influence = 75
				}
			}
		}
		if = {
			limit = { NOT = { government = republic } }
			change_government = theocracy
		}
		add_government_reform = magocracy_reform
	}
}

# Culture ties weakened
country_event = {
	id = marcher.2
	title = marcher.2.t
	desc = marcher.2.d
	picture = EUROPEAN_REFUGEES_eventPicture
	
	trigger = {
		has_country_flag = adventurer_derived_government
		NOT = { has_country_flag = new_adventurer_culture }
		NOT = { has_country_modifier = legacy_of_adventurers_modifier }
		
		num_of_cities = 5
		
		is_at_war = no
		is_vassal = no
	}
	
	mean_time_to_happen = {
		months = 200
		modifier = {
			factor = 0.7
			stability = 1
		}
		modifier = {
			factor = 0.25
			num_of_cities = 10
		}
		modifier = {
			factor = 0.1
			in_golden_age = yes
		}
	}
	
	option = { # Luciande
		name = marcher.2.a
		trigger = {
			tag = B34
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = luciander
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = luciander
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = luciander
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = luciander
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = luciander
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Ancardia
		name = marcher.2.a
		trigger = {
			tag = B35
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = ancardian
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = ancardian
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = ancardian
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = ancardian
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = ancardian
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Rogieria
		name = marcher.2.a
		trigger = {
			tag = B37
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = rogieran
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = rogieran
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = rogieran
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = rogieran
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = rogieran
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Elikhand
		name = marcher.2.a
		trigger = {
			tag = B38
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = elikhander
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = elikhander
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = elikhander
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = elikhander
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = elikhander
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Wyvernheart
		name = marcher.2.a
		trigger = {
			tag = B39
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = heartman
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = heartman
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = heartman
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = heartman
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = heartman
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Alenor
		name = marcher.2.a
		trigger = {
			tag = B40
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = alenori
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = alenori
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = alenori
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = alenori
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = alenori
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Stalbor
		name = marcher.2.a
		trigger = {
			tag = B47
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = stalboric
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = stalboric
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = stalboric
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = stalboric
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = stalboric
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Ravenmarch
		name = marcher.2.a
		trigger = {
			tag = B48
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = ravenmarcher
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = ravenmarcher
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = ravenmarcher
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = ravenmarcher
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = ravenmarcher
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Araionn
		name = marcher.2.a
		trigger = {
			tag = B49
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = ionnic
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = ionnic
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = ionnic
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = ionnic
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = ionnic
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Newshire
		name = marcher.2.a
		trigger = {
			tag = B50
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = newfoot_halfling
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = newfoot_halfling
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = newfoot_halfling
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = newfoot_halfling
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = newfoot_halfling
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Estaire
		name = marcher.2.a
		trigger = {
			tag = B51
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = estairey
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = estairey
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = estairey
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = estairey
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = estairey
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Anbenland
		name = marcher.2.a
		trigger = {
			tag = B52
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = anbenlander
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = anbenlander
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = anbenlander
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = anbenlander
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = anbenlander
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Nurcestir
		name = marcher.2.a
		trigger = {
			tag = B53
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = nurcestiran
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = nurcestiran
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = nurcestiran
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = nurcestiran
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = nurcestiran
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Esthil
		name = marcher.2.a
		trigger = {
			tag = B54
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = esthili
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = esthili
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = esthili
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = esthili
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = esthili
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Rosande
		name = marcher.2.a
		trigger = {
			tag = Z35
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = rosanda
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = rosanda
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = rosanda
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = rosanda
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = rosanda
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Covenblad
		name = marcher.2.a
		trigger = {
			tag = Z37
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = covenbladic
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = covenbladic
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = covenbladic
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = covenbladic
		}
		
		set_country_flag = new_adventurer_culture
		
		change_primary_culture = covenbladic
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	

	option = { # Resist cultural change
		name = marcher.2.b
		
		add_prestige = -10
		
		set_country_flag = new_adventurer_culture
	}
}