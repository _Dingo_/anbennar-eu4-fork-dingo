government = monarchy
add_government_reform = blademarches_kingdom_reform
government_rank = 2
primary_culture = marcher
religion = regent_court
technology_group = tech_cannorian
capital = 817
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }