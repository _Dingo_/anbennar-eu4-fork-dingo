#389 - Mukalla

owner = F08
controller = F08
add_core = F08
culture = far_akani
religion = mother_akan

hre = no

base_tax = 4
base_production = 5
base_manpower = 4

trade_goods = sugar

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish