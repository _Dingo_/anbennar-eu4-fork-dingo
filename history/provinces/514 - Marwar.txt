#514 -

owner = F11
controller = F11
add_core = F11
add_core = F16
culture = ourdi
religion = regent_court


base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_nationalism = 10