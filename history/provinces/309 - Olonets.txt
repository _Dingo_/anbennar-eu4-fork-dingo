# No previous file for Olonets
owner = A99
controller = A99
add_core = A99
culture = esmari
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = glass

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish