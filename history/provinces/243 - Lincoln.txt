#243 - Lincoln |

owner = A13
controller = A13
add_core = A13
culture = gawedi
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 4

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
