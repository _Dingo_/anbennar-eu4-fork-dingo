# No previous file for Ratris
owner = H01
controller = H01
add_core = H01
culture = selphereg
religion = eordellon
capital = "Ratris"

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = grain

native_size = 14
native_ferocity = 6
native_hostileness = 6