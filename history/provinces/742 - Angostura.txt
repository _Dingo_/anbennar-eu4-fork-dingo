#742

owner = B44
controller = B44
add_core = B44
culture = common_goblin
religion = goblinic_shamanism


base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = iron

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari