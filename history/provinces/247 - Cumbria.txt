#247 - Cumbria
#Cumberland and Westmoreland

owner = A46
controller = A46
add_core = A46
culture = arbarani
religion = regent_court

hre = no

base_tax = 3
base_production = 2
base_manpower = 1

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
