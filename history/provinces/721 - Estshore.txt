# No previous file for Estshore
owner = Z32
controller = Z32
add_core = Z32
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold