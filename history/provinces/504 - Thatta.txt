# No previous file for Thatta
owner = F05
controller = F05
add_core = F05
add_core = F01
culture = kheteratan
religion = khetist

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = livestock

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari