# No previous file for Jirem
owner = Z30
controller = Z30
add_core = Z30
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 4
base_production = 5
base_manpower = 3

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold