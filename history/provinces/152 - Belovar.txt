#152 - Varazd

owner = A96
controller = A96
add_core = A96
culture = high_lorentish
religion = regent_court
hre = no
base_tax = 6
base_production = 5
trade_goods = cloth
base_manpower = 3
capital = "" 
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
