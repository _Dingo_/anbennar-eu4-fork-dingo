# No previous file for Bahamas
owner = F09
controller = F09
add_core = F09
culture = sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_salahadesi
discovered_by = tech_gnollish