# No previous file for Kawali
owner = F25
controller = F25
add_core = F25
culture = sun_elf
religion = bulwari_sun_cult

hre = no

base_tax = 10
base_production = 7
base_manpower = 5

trade_goods = paper
center_of_trade = 2

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari