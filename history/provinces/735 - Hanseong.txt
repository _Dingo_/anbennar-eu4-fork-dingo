# No previous file for Hanseong
owner = Z18
controller = Z18
add_core = Z18
culture = gray_orc
religion = great_dookan
capital = ""


hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = unknown

native_size = 28
native_ferocity = 8
native_hostileness = 8