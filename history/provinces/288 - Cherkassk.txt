#288 - Cherkassk

owner = A33
controller = A33
add_core = A33
culture = vernman
religion = regent_court

hre = yes

base_tax = 7
base_production = 6
base_manpower = 5

trade_goods = cloth
center_of_trade = 1

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_gnollish